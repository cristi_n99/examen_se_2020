public class Ex1 {


}
interface C
{
}
class F
{
    public void metA(){}
}
class E implements C
{
    F f;
    E()
    {
        f=new F();//Composition
    }
    public void metG(int i){}
}
class B
{
    private long t;
    D d;
    E e;
    B(D d, E e, long t)
    {
        this.t=t;
        this.d=d; //Aggregation
        this.e=e; //Association
    }
    public void x(){}


}
class D
{
}
class A
{
    public void met(B b){}
}

