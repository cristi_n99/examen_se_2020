package ex2;

public class Counter extends Thread {

    Counter(String name){
        super(name);
    }

    public void run(){
        for(int i=0;i<=12;i++){
            System.out.println(getName() + " - "+i);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println(getName() + " 12 messages displayed.");
    }

    public static void main(String[] args) {
        Counter c1 = new Counter("AThread1");
        Counter c2 = new Counter("AThread2");

        c1.start();
        c2.start();

    }
}