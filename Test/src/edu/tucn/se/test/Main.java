package edu.tucn.se.exam;


import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.Random;

public class Main {


    public static void main(String[] args) {

        Bank b1 = new Bank();
        ATM a1 = new ATM(b1);
        Win win = new Win(a1);



    }

}

class Win extends JFrame {
    ATM a1;
    JLabel label1;

    Win(ATM a1) {

        this.a1 = a1;
        int[] number = new int[16];
        int[] pin = new int[4];
        int funds;

        JPanel p1 = new JPanel();
        JTextField textField1 = new JTextField();
        JTextField textField2 = new JTextField();
        JButton button1 = new JButton("Enter");

        p1.setLayout(new GridLayout(4, 1));
        p1.setBounds(0, 0, 400, 400);
        label1 = new JLabel();


        JPanel p2 = new JPanel();
        JTextField textField3= new JTextField();
        JTextField textField4= new JTextField("Enter sum to withdraw");
        JButton button2=new JButton("Check funds");
        JButton button3=new JButton("Withdraw funds");
        JButton button4=new JButton("Exit");

        p2.setLayout(new GridLayout(4, 1));
        p2.setBounds(0, 0, 400, 400);

        setSize(400, 400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);





        button1.addActionListener(e -> {

            String aux = textField1.getText();
            String aux2 = textField2.getText();

            for (int i = 0; i < 4; ++i) {
                String s = "" + aux2.charAt(i);
                pin[i] = Integer.parseInt(s);
            }
            for (int i = 0; i < 16; ++i) {
                String s = "" + aux.charAt(i);
                number[i] = Integer.parseInt(s);
            }
            if (a1.verify(number, pin)) {
                p1.setVisible(false);
                p2.setVisible(true);
            }
            else{
                label1.setText("Invalid account");
                repaint();
            }

        });
        button2.addActionListener(e->{
            textField3.setText(a1.balanceCheck());
        });
        button3.addActionListener(e->{
            String aux = textField4.getText();

            ArrayList<Integer> cash=a1.cashOut(Integer.parseInt(aux));
            if(cash==null)
            {
                textField3.setText("Insuficient funds!");
            }
            else
            {
                textField3.setText("");
                cash.forEach(c->{
                    textField3.setText(textField3.getText()+c+"$ ");
                });
            }

        });
        button4.addActionListener(e->{
            a1.number=null;
            p2.setVisible(false);
            p1.setVisible(true);
        });

        add(p1);
        add(p2);
        p1.add(button1);
        p1.add(textField1);
        p1.add(textField2);
        p1.add(label1);
        p2.add(button2);
        p2.add(button3);
        p2.add(button4);
        p2.add(textField3);
        p2.add(textField4);


        setVisible(true);
    }

}


class Account {
    private int[] pin;
    private int balance;

    public Account(int[] pin, int balance) {
        this.pin = pin;
        this.balance = balance;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}

class Bank {
    private HashMap<int[], Account> accounts = new HashMap<>();
    public ArrayList<Integer> bills = new ArrayList<>();

    Bank() {
        bills.add(50);
        bills.add(10);
        bills.add(5);
        bills.add(1);
    }

    void createAccount(int[] number, int[] pin) {
        Random r1 = new Random();
        accounts.put(number, new Account(pin, r1.nextInt(300) + 100));
    }

    int getBalance(int[] number) {
        return accounts.get(number).getBalance();
    }

    void setBalance(int[] number, int balance) {
        accounts.get(number).setBalance(balance);
    }

}

class ATM {
    Bank b1;



    int[] number = null;

    public ATM(Bank b1) {
        this.b1 = b1;

    }

    boolean verify(int[] number, int[] pin) {
        Random r1 = new Random();

        if (r1.nextInt(4) != 1) {
            b1.createAccount(number, pin);
            this.number = number;
            return true;
        } else {

            return false;
        }
    }

    String balanceCheck() {

        return "You currently have: " + b1.getBalance(this.number) + "$.";
    }

    ArrayList<Integer> cashOut(int v) {
        if (number == null)
            return null;
        if (b1.getBalance(this.number) - v < 0) {

            return null;
        } else {
            b1.setBalance(this.number, b1.getBalance(this.number) - v);
        }

        ArrayList<Integer> value = new ArrayList<>();
        value.add(v);
        ArrayList<Integer> aux = new ArrayList<>();
        b1.bills.forEach(bill -> {
            while (value.get(0) - bill >= 0) {
                aux.add(bill);
                value.set(0, value.get(0) - bill);
            }
        });
        aux.forEach(System.out::println);
        return aux;

    }



    void exit() {
        this.number = null;
    }
}


